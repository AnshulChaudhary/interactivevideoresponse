from django.contrib import admin
from django.utils.html import format_html

from .models import interview_setup_validate, candidate_details, interview_details, interview_questions, interview_interviewer_map, candidate_interviewer_excel

import os
# Register your models here.

class interview_detailsAdmin(admin.ModelAdmin):
    list_display = ('company_id', 'interview_name', 'contact', 'start_date', 'end_date', 'interview_status',)
    search_fields = ('interview_name',)
    ordering = ('company_id', 'interview_name',)
    list_filter = ('company_id',)

admin.site.register(interview_details, interview_detailsAdmin)

class candidate_detailsAdmin(admin.ModelAdmin):
    readonly_fields = ('interview_code','candidate_ip',)
    list_display = ('first_name', 'last_name', 'email', 'interview_id', 'question_answered', 'submission_status', 'link_status',)
    search_fields = ('first_name', 'last_name', 'email',)
    ordering = ('interview_id', 'first_name', 'last_name',)
    list_filter = ('interview_id','submission_status', 'link_status',)

admin.site.register(candidate_details, candidate_detailsAdmin)

class interview_questionsAdmin(admin.ModelAdmin):
    list_display = ('interview_id', 'interview_question')
    search_fields = ()
    ordering = ('interview_id', 'interview_question')
    list_filter = ('interview_id',)

admin.site.register(interview_questions, interview_questionsAdmin)

class interview_interviewer_mapAdmin(admin.ModelAdmin):
    list_display = ('interviewer_id', 'interview_id')
    search_fields = ()
    ordering = ('interview_id', 'interviewer_id')
    list_filter = ('interview_id',)

admin.site.register(interview_interviewer_map, interview_interviewer_mapAdmin)

class interview_setup_validateAdmin(admin.ModelAdmin):
    list_display = ('company_id', 'contact', 'setup_code', 'link_status')
    search_fields = ()
    ordering = ('company_id',)
    list_filter = ('link_status', 'company_id')

admin.site.register(interview_setup_validate,interview_setup_validateAdmin)

class candidate_interviewer_excelAdmin(admin.ModelAdmin):
    readonly_fields = ('candidate_excel','interviewer_excel')
    list_display = ('interview_id', 'download_candidate', 'download_interviewer')
    search_fields = ()
    ordering = ()
    list_filter = ()

    def download_candidate(self, obj):
        return format_html("<a href='/media/{}'>Download</a>".format(obj.candidate_excel))

    def download_interviewer(self, obj):
        return format_html("<a href='/media/{}'>Download</a>".format(obj.interviewer_excel))

    download_candidate.short_description = "Candidate List"
    download_interviewer.short_description = "Interviewer List"
admin.site.register(candidate_interviewer_excel,candidate_interviewer_excelAdmin)
