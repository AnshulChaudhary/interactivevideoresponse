from django.apps import AppConfig


class CompanydashboardConfig(AppConfig):
    name = 'CompanyDashboard'
