from django.db import models
from datetime import datetime, timedelta

from account.models import MyUser, company_details
# Create your models here.


class interview_details(models.Model):
    interview_name = models.CharField(max_length=255)
    contact = models.EmailField(max_length=100)
    interview_status = models.BooleanField(default=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    time_Zone = models.CharField(max_length=10,default='0')
    question_count = models.IntegerField()
    preparation_time = models.IntegerField(help_text='time in sec')
    interview_duration = models.IntegerField(help_text='time in sec')
    attempts_per_question = models.IntegerField(default=1)
    company_id = models.ForeignKey(company_details,on_delete=models.CASCADE)

    def __str__ (self):
        return self.interview_name


class candidate_details(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    interview_code = models.CharField(max_length=20)
    question_answered = models.IntegerField(default=0)
    submission_status = models.BooleanField(default=False)
    link_status = models.BooleanField(default=True)
    candidate_ip = models.CharField(max_length=20, null=True, default=None)
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)

    def __str__(self):
        return self.email

class interview_questions(models.Model):
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)
    # question_number = models.IntegerField()
    interview_question = models.CharField(max_length=255)

    def __str__(self):
        return self.interview_question


class interview_setup_validate(models.Model):
    contact = models.EmailField(max_length=100)
    setup_code = models.CharField(max_length=20)
    link_status = models.BooleanField(default=True)
    company_id = models.ForeignKey(company_details,on_delete=models.CASCADE)
    

class interview_interviewer_map(models.Model):
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)
    interviewer_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)

def excel_path(instance, filename):
    return "{}/{}/Excel Sheets/{}".format(instance.interview_id.company_id,instance.interview_id.interview_name,filename)
class candidate_interviewer_excel(models.Model):
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)
    candidate_excel = models.FileField(upload_to=excel_path)
    interviewer_excel = models.FileField(upload_to=excel_path)

    def delete(self, *args, **kwargs):
        self.candidate_excel.delete()
        self.interviewer_excel.delete()
        super().delete(*args, **kwargs)
