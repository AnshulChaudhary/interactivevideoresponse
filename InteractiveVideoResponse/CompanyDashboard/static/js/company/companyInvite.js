$("#close").on("click", function(){
    console.log("clicking")
    $("#invite-modal").css("display","none")
})
//preview invite
$(document).ready(function(){
    $('#preview').click(function (e){
        if (isEmpty($('#company_name_pre').val())){
            $('#companyNameError').text("Company Name can't be empty")
            var error = "&#9888;"
            $('#companyNameError').prepend(error)
            $('#companyNameError').addClass("is-visible")
            $("#company_name_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(!validateEmail($('#contact_pre').val())){
            $('#contactError').text("Invalid Email");
            var error = "&#9888;"
            $('#contactError').prepend(error)
            $('#contactError').addClass("is-visible")
            $("#contact_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else{
            $('#companyNameError').text("")
            $("#company_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#contactError').text("")
            $("#contactpre").css("border-color","rgba(0,0,0,0.75)")
            $("#invite-modal").css("display","block")
            
            $('#company_name').attr('value',$('#company_name_pre').val())
            $('#company_name').attr('readonly',true)
            // $('#company_name')[0].readOnly = true;
            $('#contact').attr('value',$('#contact_pre').val())
            $('#contact').attr('readonly',true);

            // $('#preview')[0].style.display = 'none';
            // $('#edit')[0].style.display = 'inline';
            // $('#sendInvite')[0].style.display = 'inline';
            // $('#edit')[0].disabled = false;
            // $('#sendInvite')[0].disabled = false
        }
    });
});

//edit invite
$(document).ready(function(){
    $('#edit').click(function (e){
        // $('#company_name')[0].readOnly = false;
        // $('#contact')[0].readOnly = false;
        $("#invite-modal").css("display","none")
        // $('#preview')[0].style.display = 'inline';
        // $('#edit')[0].style.display = 'none';
        // $('#sendInvite')[0].style.display = 'none';
        // $('#edit')[0].disabled = true;
        // $('#sendInvite')[0].disabled = true
    });
});

//empty check
function isEmpty(value){
    if (value === '' || value === null || value === undefined) {
        return true
    } else {
        return false
    }
}

//email check
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
