$(document).ready(function () {
    $('.navbar_button').click(function () {
        $('ul').addClass('ul_show');
        $('#close').css("visibility","visible");
    });
    $('#close').click(function(){
        $('ul').removeClass('ul_show');
    });
    $('li').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
});
