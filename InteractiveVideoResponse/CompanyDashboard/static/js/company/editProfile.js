$(document).ready(function () {
    $('#edit-button').on('click', function () {
        $('#fname').attr('readonly',false);
        $('#lname').attr('readonly',false);
        $('#email').attr('readonly',false);
        $('#edit-button').css('display','none');
        $('#submit-button').css('display','block');
    });    
});
