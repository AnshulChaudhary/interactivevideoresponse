
// dynamic question input loader
$(document).ready(function(){
    $('#add').click(function (e){
        question_count = $("#question_count_pre").val();
        if (question_count == 0){
            var text = 'Question count is Empty';
            $('#questionError').text(text)
            var error = "&#9888;"
            $('#questionError').prepend(error)
            $('#questionError').addClass("is-visible")
            $("#question_count").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else{
            $('#questionError').text('')
            $('#questions_pre').text('')
            $("#question_count").css("border-color","rgba(0,0,0,0.75)")
            $("#questions_pre").append('<label class="form-group">Enter Question 1</label>', '<input type="text" class="form-control" id="question1_pre" name="question1">', '<br>');
            for (var i = 2; i <= question_count; i++){
                var label = '<label class="form-group">Enter Question '+i+'</label> ';
                var br = '<br>'; 
                var input = '<input type="text" class="form-control" id="question'+i+'_pre" name="question'+i+'">';
                $("#questions_pre").append(label, input, br);
            }
        }
    });
});

var candidate_file = document.querySelectorAll( '#candidateList_pre' );
Array.prototype.forEach.call( candidate_file, function( input )
{
	var label = input.previousElementSibling
        // labelVal = label.innerHTML;
	input.addEventListener( 'change', function( e )
	{
        var fileName = '';
        fileName = e.target.value;
        fileName = fileName.replace(/.*[\/\\]/, '');
        console.log(label)
        label.innerHTML = fileName;
	});
});

var interviewer_file = document.querySelectorAll( '#interviewerList_pre' );
Array.prototype.forEach.call( interviewer_file, function( input )
{
	var label = input.nextElementSibling,
        labelVal = label.innerHTML;
	input.addEventListener( 'change', function( e )
	{
        var fileName = '';
        fileName = e.target.value;
        fileName = fileName.replace(/.*[\/\\]/, '');
        label.innerHTML = fileName;
	});
});


// check if file is Excel
function checkCandidate(){
    var fileInput = document.getElementById("candidateList_pre");
    var filePath = fileInput.value;
    var allowedExtensions = /(\.xlsx|\.xls)$/i;
    if(!allowedExtensions.exec(filePath)){
        var text = 'Please upload file having extensions .xlsx/.xls only.';
        $('#candidateError').text(text)
        var error = "&#9888;"
        $('#candidateError').prepend(error)
        $('#candidateError').addClass("is-visible")
        $('#candidateList_pre_label').addClass("text")
        fileInput.value = '';
        return false;
    }
    else{
        $('#candidateError').text('')
        if($('#candidateList_pre_label').hasClass("text")){
            $('#candidateList_pre_label').removeClass("text")
        }
        CandidateExcelCheck(fileInput.files)
        return true
    }
}
function checkInterviewer(){
    var fileInput = document.getElementById("interviewerList_pre");
    var filePath = fileInput.value;
    var allowedExtensions = /(\.xlsx|\.xls)$/i;
    if(!allowedExtensions.exec(filePath)){
        var text = 'Please upload file having extensions .xlsx/.xls only.';
        $('#interviewerError').text(text)
        var error = "&#9888;"
        $('#interviewerError').prepend(error)
        $('#interviewerError').addClass("is-visible")
        $('#interviewerList_pre_label').addClass("text")
        fileInput.value = '';
        return false;
    }
    else{
        $('#interviewerError').text('')
        if($('#interviewerList_pre_label').hasClass("text")){
            $('#interviewerList_pre_label').removeClass("text")
        }
        InterviewerExcelCheck(fileInput.files)
        return true
    }
}


// Excel file column check candidate
var CandidateDetailsCheck = function() {
    this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });
            workbook.SheetNames.forEach(function(sheetName) {
                var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                var json_object = JSON.stringify(XL_row_object);
                var list = JSON.parse(json_object);
                var keys = Object.keys(list[0]);
                if(!list[0].hasOwnProperty('first_name') || !list[0].hasOwnProperty('last_name') || !list[0].hasOwnProperty('email') ){
                    var text = 'Invalid format: colums required(first_name, last_name, email)';
                    $('#candidateError').text(text);
                    var error = "&#9888;"
                    $('#candidateError').prepend(error)
                    $('#candidateError').addClass("is-visible")
                    $('#candidateList_pre_label').addClass("text")
                    document.getElementById("candidateList_pre").value = '';
                }
                else{
                    $('#candidateError').text('');
                    if($('#candidateList_pre_label').hasClass("text")){
                        $('#candidateList_pre_label').removeClass("text")
                    }
                }
            })
        };
        reader.onerror = function(ex) {
            console.log(ex);
        };
        try {
            reader.readAsBinaryString(file);
        } catch (error) {
            console.log('File error');
        }
    };
};
function CandidateExcelCheck(files) {
    var candidateCheck = new CandidateDetailsCheck();
    candidateCheck.parseExcel(files[0]);
}


// Excel file column check candidate
var InterviewerDetailsCheck = function() {
    this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });
            workbook.SheetNames.forEach(function(sheetName) {
                var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                var json_object = JSON.stringify(XL_row_object);
                var list = JSON.parse(json_object);
                var keys = Object.keys(list[0]);
                if(!list[0].hasOwnProperty('first_name') || !list[0].hasOwnProperty('last_name') || !list[0].hasOwnProperty('email') ){
                    var text = 'Invalid format: columns required(first_name, last_name, email)';
                    $('#interviewerError').text(text);

                    document.getElementById("candidateList_pre").value = '';
                }
                else{
                    $('#interviewerError').text('');
                    if($('#interviewerList_pre_label').hasClass("text")){
                        $('#interviewerList_pre_label').removeClass("text")
                    }
                }
            })
        };
        reader.onerror = function(ex) {
            console.log(ex);
        };
        try {
            reader.readAsBinaryString(file);
        } catch (error) {
            console.log('File error');
        }
    };
};
function InterviewerExcelCheck(files) {
    var candidateCheck = new InterviewerDetailsCheck();
    candidateCheck.parseExcel(files[0]);
}


// form preview
$(document).ready(function(){
    $('#preview').click(function (e){
        if (isEmpty($('#interview_name_pre').val())){
            $('#interviewNameError').text("Interview Name can't be empty")
            var error = "&#9888;"
            $('#interviewNameError').prepend(error)
            $('#interviewNameError').addClass("is-visible")
            $("#interview_name_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(isEmpty($('#start_date_pre').val())){
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("Start Date can't be empty")
            var error = "&#9888;"
            $('#startDateError').prepend(error)
            $('#startDateError').addClass("is-visible")
            $("#start_date_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(isEmpty($('#end_date_pre').val())){
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("")
            $("#start_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#endDateError').text("End Date can't be empty");
            var error = "&#9888;"
            $('#endDateError').prepend(error)
            $('#endDateError').addClass("is-visible")
            $("#end_date_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(isEmpty($('#preparationTime').val())){
            $('#interviewNameError').text("")
            $('#startDateError').text("")
            $('#endDateError').text("")
            $('#preparationError').text("Enter preparation time")
        }
        else if(isEmpty($('#answerTime').val())){
            $('#interviewNameError').text("")
            $('#startDateError').text("")
            $('#endDateError').text("")
            $('#preparationError').text("")
            $('#answerError').text("Enter preparation time")
        }
        else if(isEmpty($('#question1_pre').val())){
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("")
            $("#start_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#endDateError').text("")
            $("#end_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#question1Error').text("Questions can't be empty");
            var error = "&#9888;"
            $('#question1Error').prepend(error)
            $('#question1Error').addClass("is-visible")
            $("#question1_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(isEmpty($('#candidateList_pre').val())){
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("")
            $("#start_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#endDateError').text("")
            $("#end_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#question1Error').text("");
            $("#question1_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#candidateError').text("Upload Candidate Excel Sheet");
            var error = "&#9888;"
            $('#candidateError').prepend(error)
            $('#candidateError').addClass("is-visible")
            // $("#candidateList_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else if(isEmpty($('#interviewerList_pre').val())){
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("")
            $("#start_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#endDateError').text("End Date can't be empty");
            $('#endDateError').text("")
            $("#end_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#question1Error').text("");
            $("#question1_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#candidateError').text("")
            // $("#candidateList_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#interviewerError').text("Upload Interviewer Excel Sheet");
            var error = "&#9888;"
            $('#interviewerError').prepend(error)
            $('#interviewerError').addClass("is-visible")
            // $("#interviewerList_pre").css({"border-width":"1px","border-color":"red","border-style":"solid"})
        }
        else{
            $('#interviewNameError').text("")
            $("#interview_name_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#startDateError').text("")
            $("#start_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#endDateError').text("")
            $("#end_date_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#question1Error').text("")
            $("#question1_pre").css("border-color","rgba(0,0,0,0.75)")
            $('#candidateError').text("")
            $('#interviewerError').text("")
            $('#setup-modal').css("display","block")
            $('#questions').remove()
            $('#company_name').attr('value',$('#company_name_pre').val())
            $('#company_name').attr('readonly',true)
            $('#interview_name').attr('value',$('#interview_name_pre').val())
            $('#interview_name').attr('readonly',true)
            $('#start_date').attr('value',$('#start_date_pre').val())
            $('#start_date').attr('readonly',true)
            $('#end_date').attr('value',$('#end_date_pre').val())
            $('#end_date').attr('readonly',true)            
            $('#question_count').attr('value',$('#question_count_pre').val())
            $('#question_count').attr('readonly',true)
            var questions = '<div id="questions"></div>'
            $('#questions_add').append(questions);
            for (var i = 1; i <= $('#question_count_pre').val(); i++){
                var label = '<label class="form-group">Question '+i+'</label> ';
                var br = '<br>'; 
                var input = '<input type="text" class="form-control question'+i+'" id="question'+i+'">';
                $("#questions").append(label, input, br);
            }

            // if(temp_question_count == $('#question_count_pre').val()){
                
            // }
            // else{
            //     var count = $('#question_count_pre').val() - temp_question_count
            //     for (var i = temp_question_count+1; i <= $('#question_count_pre').val(); i++){
            //         var label = '<label class="form-group">Question '+i+'</label> ';
            //         var br = '<br>'; 
            //         var input = '<input type="text" class="form-control question'+i+'" id="question'+i+'">';
            //         $("#questions").append(label, input, br);
            //     }
            // }

            for (var i = 1; i <= $('#question_count_pre').val(); i++){
                var temp = 'question'+i;
                $('#'+temp).attr('value',$('#'+temp+'_pre').val())
                $('#'+temp).attr('readonly',true)
            }
            $('#candidateList').attr('value',$('#candidateList_pre').val())
            // $('#candidateList').attr('disabled',true)
            $('#candidateList_label').attr('readonly',true)
            var candidate_fileName = $('#candidateList_pre').val()
            candidate_fileName = candidate_fileName.replace(/.*[\/\\]/, '');
            $('#candidateList_label').text(candidate_fileName)
            $('#interviewerList').attr('value',$('#interviewerList_pre').val())
            // $('#interviewerList').attr('disabled',true)
            $('#interviewerList_label').attr('readonly',true)
            var interviewer_fileName = $('#interviewerList_pre').val()
            interviewer_fileName = interviewer_fileName.replace(/.*[\/\\]/, '');
            $('#interviewerList_label').text(interviewer_fileName)
            console.log($('#candidateList_pre').val())
            

            
            
            // $('#preview')[0].style.display = 'none';
            // $('#edit')[0].style.display = 'inline';
            // $('#finalSubmit')[0].style.display = 'inline';
            // $('#edit')[0].disabled = false;
            // $('#finalSubmit')[0].disabled = false;
        }
    });
});


//empty check
function isEmpty(value){
    if (value === '' || value === null || value === undefined) {
        return true
    } else {
        return false
    }
}


//form edit
$(document).ready(function(){
    
    $('#edit').click(function (e){
        $("#setup-modal").css("display","none")
        // $('#company_name')[0].readOnly = false;
        // $('#interview_name')[0].readOnly = false;
        // $('#start_date')[0].readOnly = false;
        // $('#end_date')[0].readOnly = false;
        // $('#question_count')[0].readOnly = false;
        // $('#add')[0].readOnly = false;
        // for (var i = 1; i <= $('#question_count').val(); i++){
        //     var temp = 'question'+i;
        //     $('#'+temp)[0].readOnly = false;
        // }
        // $('#candidateList')[0].readOnly = false;
        // $('#interviewerList')[0].readOnly = false;
        // $('#preview')[0].style.display = 'inline';
        // $('#edit')[0].style.display = 'none';
        // $('#finalSubmit')[0].style.display = 'none';
        // $('#edit')[0].disabled = true;
        // $('#finalSubmit')[0].disabled = true;
    });
    $('interviewSetup-form').on("submit",function (e){
        $('#candidateList').attr('disabled',false)
        $('#interviewerList').attr('disabled',false)
    });
});




