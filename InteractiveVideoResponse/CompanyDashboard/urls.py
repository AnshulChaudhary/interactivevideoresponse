from django.urls import path, include
from django.conf.urls import url

from . import views

urlpatterns = [
    path('interview/company/invite/', views.companyInvite, name='Company Invite'),
    path('interviewSetup/<setup_code>/', views.addInterview, name='Interview Setup'),
    path('createInterview/<setup_code>/', views.createInterview, name='Create Interview'),
    # path('upload', views.CompanyDir),
    path('dashboard/', views.companyDashboard, name='companyDashboard'),
    path('video/', views.companyVideo, name='companyVideo'),
    path('candidateVideo/', views.companyCandidateVideo, name='companyCandidateVideo'),
    path('singlevideo/', views.companySingleVideo, name='companySingleVideo'),
    path('myInterview/', views.myInterview, name='myInterview'),
    path('profile/', views.profile, name='profile'),
    path('changePassword/', views.changePassword, name='changePassword'),
]