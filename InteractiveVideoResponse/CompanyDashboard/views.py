from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, auth
from django.contrib import messages

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template


from django.contrib.auth.decorators import login_required

import random
import string

from account.models import MyUser, company_details
from VideoResponse.models import submission_attempts
from .models import interview_setup_validate, candidate_details, interview_details, interview_questions, interview_interviewer_map, candidate_interviewer_excel

from ipware import get_client_ip
import pandas as pd
import os
from pandas import ExcelWriter
# Create your views here.


#setup_code generation
def setup_code(stringLength=20):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

#generate pasword
def generatePassword(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

#send mail
def inviteMail(code, contact): 
    setupLink = 'http://127.0.0.1:8000/interviewSetup/'
    setupLink=setupLink+code
    plaintext = get_template('company/invite.txt')
    d = {'setupLink' : setupLink}
    subject = 'To setup Interview' 
    from_email = 'noreply@example.com'
    to_email = [contact]
    text_content = plaintext.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
    msg.send()
    return

# coordinator login mail
def newUserEmail(contact, password):
    plaintext = get_template('company/idPassword.txt')
    d = {'email': contact, 'password': password}
    subject = 'To setup Interview' 
    from_email = 'noreply@example.com'
    to_email = [contact]
    text_content = plaintext.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
    msg.send()
    return

#admin only for inviting company
@login_required
def companyInvite(request):
    if request.method == 'GET':
        if request.user.is_admin:
            return render(request,'company/companyInvite.html')
        else:
            return render(request, '404error.html')
    elif request.method == 'POST':
        company_name = request.POST['company_name'].strip()
        first_name = request.POST['first_name'].strip()
        last_name = request.POST['last_name'].strip()
        contact = request.POST['contact'].strip()

        #Check if company exists
        if not company_details.objects.filter(company_name=company_name).exists():
            try:
                company = company_details.objects.create(
                    company_name = company_name,
                    live_interview = 0,
                    total_interview = 0
                )
                company.save()
            except Exception as e:
                print(e)

        company = company_details.objects.get(company_name=company_name)

        #interview setup code
        global code 
        code = setup_code()
        while(interview_setup_validate.objects.filter(setup_code=code).exists()):
            code = setup_code()
        try:
            invitesetup = interview_setup_validate.objects.create(
                contact=contact, 
                setup_code=code, 
                link_status=True,
                company_id=company
            )
            invitesetup.save()
        except Exception as e:
            print(e)

        # send email for interview setup
        inviteMail(code, contact)

        # coordinator created
        if not MyUser.objects.filter(email=contact).exists():
            global password
            password = generatePassword()
            user = MyUser.objects.create_user(
                first_name=first_name, 
                last_name=last_name, 
                email=contact, 
                password=password,
                is_coordinator=True,
                company_id = company
            )
            # send mail with coordinator details
            newUserEmail(contact, password)

        messages.info(request, 'Invite sent to '+company_name)
        return redirect('/')
    else:
        return render(request, '404error.html')


#company to add interview
def addInterview(request, setup_code):
    if request.method == 'GET' and interview_setup_validate.objects.filter(setup_code=setup_code).exists():
        interviewObj = interview_setup_validate.objects.get(setup_code=setup_code)
        if interviewObj.link_status:
            return render(request, 'company/interviewSetup.html', {'company_name': interviewObj.company_id.company_name, 'setup_code': interviewObj.setup_code})
        else:
            return render(request, 'company/interviewSetupExpire.html')
    else:
        return render(request, '404error.html')

# generate candidate code
def candidateCode(stringLength=20):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


#upload the candidate list 
def uploadCandidate(candidateExcel, interview_id):
    candidateList = pd.read_excel(candidateExcel)
    interview = interview_details.objects.get(id = interview_id)
    for index, row in candidateList.iterrows():
        tempFirstName = row['first_name'].strip()
        tempLastName = row['last_name'].strip()
        tempEmail = row['email'].strip()
        global tempCode
        tempCode = candidateCode()
        while(candidate_details.objects.filter(interview_code=tempCode).exists()):
            tempCode = candidateCode()
        tempCandidate = candidate_details.objects.create(
            first_name = tempFirstName,
            last_name = tempLastName,
            email = tempEmail,
            interview_code = tempCode,
            interview_id = interview
        )
        tempCandidate.save()
    return

#upload the interviewer list 
def uploadInterviewer(interviewerExcel, interview_id, company_id):
    interviewerList = pd.read_excel(interviewerExcel)
    company = company_details.objects.get(id = company_id)
    interview = interview_details.objects.get(id = interview_id)
    for index, row in interviewerList.iterrows():
        tempFirstName = row['first_name'].strip()
        tempLastName = row['last_name'].strip()
        tempEmail = row['email'].strip()
        try:
            if not MyUser.objects.filter(email=tempEmail).exists():
                global password
                password = generatePassword()
                tempUser = MyUser.objects.create_user(
                    first_name=tempFirstName, 
                    last_name=tempLastName, 
                    email=tempEmail, 
                    password=password,
                    company_id = company,
                    is_coordinator=False
                )
                tempUser.save()
                newUserEmail(tempEmail, password)
            user = MyUser.objects.get(email=tempEmail)
            interview_interviewer_map.objects.create(
                interview_id=interview,
                interviewer_id=user
            ).save()
        except Exception as e:
            print(e)
    return

# upload excel
def upload_excel(candidateExcel, interviewerExcel, company_name, interview_name):
    try:
        interview = interview_details.objects.get(interview_name=interview_name)
        excel = candidate_interviewer_excel.objects.create(interview_id = interview, 
        candidate_excel = candidateExcel,
        interviewer_excel = interviewerExcel
        )
        excel.save()
    except Exception as e:
        print(e)

# upload attempts
def upload_attempts(interview_name):
    try:
        interview = interview_details.objects.get(interview_name=interview_name)
        candidate_list = candidate_details.objects.filter(interview_id = interview)
        question_list = interview_questions.objects.filter(interview_id = interview)
        for candidate in candidate_list:
            for question in question_list:
                temp_submission_attempts = submission_attempts.objects.create(
                    max_attempts = interview.attempts_per_question,
                    interview_id = interview,
                    candidate_id = candidate,
                    interview_questions = question
                )
                temp_submission_attempts.save()
    except Exception as e:
        print(e)

# add interview
def createInterview(request, setup_code):
    if request.method == 'POST':
        interviewValidate = interview_setup_validate.objects.get(setup_code=setup_code)
        if interviewValidate.link_status:
            try:
                company_name = request.POST['company_name']
                interview_name = request.POST['interview_name']
                start_date = request.POST['start_date']
                end_date = request.POST['end_date']
                question_count = int(request.POST['question_count'])
                preparationTime = int(request.POST['preparationTime'])
                answerTime = int(request.POST['answerTime'])
                candidateExcel = request.FILES['candidateList']
                interviewerExcel = request.FILES['interviewerList']
                questions = [] 
                for i in range(1,(question_count+1)):
                    j = str(i)
                    temp = 'question'+j
                    questions.append(request.POST[temp])
                company = company_details.objects.get(company_name=company_name)
                # create interview
                interview = interview_details.objects.create(
                    interview_name=interview_name,
                    contact =interviewValidate.contact,
                    interview_status=True,
                    start_date=start_date,
                    end_date=end_date,
                    question_count=question_count,
                    preparation_time=preparationTime,
                    interview_duration=answerTime,
                    company_id=company
                )
                interview.save()    
            except Exception as e:
                messages.info(request, 'Unxepected Error Pleas try again or contact us at <email here>')
                return redirect('/')
            interviewObj = interview_details.objects.get(interview_name=interview_name)
            # upload Questions
            question_number = 1
            for i in questions:
                interviewQuestions =  interview_questions.objects.create(
                    question_number = question_number,
                    interview_question=i,
                    interview_id=interviewObj
                )
                question_number = question_number + 1
            # upload candidate
            uploadCandidate(candidateExcel, interviewObj.id)
            # upload interviewer
            uploadInterviewer(interviewerExcel, interviewObj.id, company.id)
            # upload excel 
            upload_excel(candidateExcel, interviewerExcel, company_name, interview_name)
            # upload attempt table
            upload_attempts(interview_name)
            # Link deactvate
            interviewValidate.link_status=False
            interviewValidate.save()
            # update interview status in 
            liveInterview = int(company.live_interview)
            totalInterview = int(company.total_interview)
            company.live_interview = liveInterview + 1
            company.total_interview = totalInterview + 1
            company.save()
            messages.info(request, 'Interview setup done, invite sent to candidates and Interviewer.')
            return redirect('/')
        else:
            return render(request, 'company/interviewSetupExpire.html')
    else:
        return render(request, '404error.html')

def companyDashboard(request):
    return render(request, "company/companyDashboard.html")

def companyVideo(request):
    return render(request, "company/companyVideo.html")

def companyCandidateVideo(request):
    return render(request, "company/companyCandidateVideo.html")

def companySingleVideo(request):
    return render(request, "company/companySingleVideo.html")

def myInterview(request):
    return render(request, "company/myInterview.html")

def profile(request):
    return render(request, 'company/companyEditProfile.html')

def changePassword(request):
    return render(request, 'company/companyChangePassword.html')
