
var scroll = window.requestAnimationFrame ||
            function(callback){ window.setTimeout(callback, 1000)};

var elementsFromUp = document.querySelectorAll('.show-on-scroll-up');
var elementsFromLeft = document.querySelectorAll('.show-on-scroll-left');
var elementsFromRight = document.querySelectorAll('.show-on-scroll-right');
var elementsFromDown = document.querySelectorAll('.show-on-scroll-down');
console.log(elementsFromUp,elementsFromDown,elementsFromRight,elementsFromLeft)
function loop() {
    elementsFromUp.forEach(function (element) {
        if (isElementInViewport(element)) {
        // element.classList.add('animate__fadeInUp');
        element.classList.add('is-visible');
        } else {
        // element.classList.remove('animate__fadeInUp');
        element.classList.remove('is-visible');
        }
    });
    elementsFromDown.forEach(function (element) {
        if (isElementInViewport(element)) {
        element.classList.add('animate__fadeInDown');
        
        } else {
        element.classList.remove('animate__fadeInDown');
      
        }
    });
    elementsFromLeft.forEach(function (element) {
        if (isElementInViewport(element)) {
        element.classList.add('animate__fadeInLeft');
        
        } else {
        element.classList.remove('animate__fadeInLeft');
      
        }
    });
    elementsFromRight.forEach(function (element) {
        if (isElementInViewport(element)) {
        element.classList.add('animate__fadeInRight');
        
        } else {
        element.classList.remove('animate__fadeInRight');
        
        }
    });
    scroll(loop);
}

loop();

function isElementInViewport(el) {
    // special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
        (rect.top <= 0
        && rect.bottom >= 0)
        ||
        (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight))
        ||
        (rect.top >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
    );
}

// var $elementsFromUp = $('.show-on-scroll-up');
// var $elementsFromDown = $('.show-on-scroll-down');
// var $elementsFromLeft = $('.show-on-scroll-left');
// var $elementsFromRight = $('.show-on-scroll-right');
// var $elementsFromIn = $('.show-on-scroll-in');
// var $window = $(window);

// function check_if_in_view() {
//   var window_height = $window.height();
//   var window_top_position = $window.scrollTop();
//   var window_bottom_position = (window_top_position + window_height);
 
//   $.each($elementsFromUp, function() {
//     var $element = $(this);
//     var element_height = $element.height();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);
 
//     //check to see if this current container is within viewport
//     if ((element_bottom_position > window_top_position) &&
//         (element_top_position < window_bottom_position)) {
//           $element.css("display","block");
//           $element.addClass('animate__fadeInUp');
          
//     } else {
//       $element.css("display","none");
//       $element.removeClass('animate__fadeInUp');
//       $element.off('scroll');
//     }
//   });
//   $.each($elementsFromDown, function() {
//     var $element = $(this);
//     var element_height = $element.outerHeight();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);
 
//     //check to see if this current container is within viewport
//     if ((element_bottom_position > window_top_position) &&
//         (element_top_position < window_bottom_position)) {
//           $element.css("display","block");
//           $element.addClass('animate__fadeInDown');
//     } else {
//       $element.css("display","none");
//       $element.removeClass('animate__fadeInDown');
//       $(element).off('scroll');
//     }
//   });
//   $.each($elementsFromLeft, function() {
//     var $element = $(this);
//     var element_height = $element.outerHeight();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);
 
//     //check to see if this current container is within viewport
//     if ((element_bottom_position > window_top_position) &&
//         (element_top_position < window_bottom_position)) {
//           $element.css("display","block");
//           $element.addClass('animate__fadeInLeft');
//     } else {
//       $element.css("display","none");
//       $element.removeClass('animate__fadeInLeft');
//       $(element).off('scroll');
//     }
//   });
//   $.each($elementsFromRight, function() {
//     var $element = $(this);
//     var element_height = $element.outerHeight();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);
 
//     //check to see if this current container is within viewport
//     if ((element_bottom_position > window_top_position) &&
//         (element_top_position < window_bottom_position)) {
//           $element.css("display","block");
//           $element.addClass('animate__fadeInRight');
//     } else {
//       $element.css("display","none");
//       $element.removeClass('animate__fadeInRight');
//       $(element).off('scroll');
//     }
//   });
//   $.each($elementsFromIn, function() {
//     var $element = $(this);
//     var element_height = $element.outerHeight();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);
 
//     //check to see if this current container is within viewport
//     if ((element_bottom_position >= window_top_position) &&
//         (element_top_position <= window_bottom_position)) {
//           $element.css("display","block");
//           $element.addClass('animate__fadeIn');
//     } else {
//       $element.css("display","none");
//       $element.removeClass('animate__fadeIn');
//       $(element).off('scroll');
//     }
//   });
// }

// $window.on('scroll resize', check_if_in_view);
// $window.trigger('scroll');



// $('#flex-1').hover(function() {
//     $('#image').fadeOut(500, function() {
//         $('#image').attr("src","/static/images/nature.jpg");
//         $('#image').fadeIn(500);
//     });
// });

// $('#flex-2').hover(function() {
//     $('#image').fadeOut(500, function() {
//         $('#image').attr("src","/static/images/water.jpg");
//         $('#image').fadeIn(500);
//     });
// });

// $('#flex-3').hover(function() {
//     $('#image').fadeOut(500, function() {
//         $('#image').attr("src","/static/images/tree.jpg");
//         $('#image').fadeIn(500);
//     });
// });

// $('#flex-4').hover(function() {
//     $('#image').fadeOut(500, function() {
//         $('#image').attr("src","/static/images/road.jpg");
//         $('#image').fadeIn(500);
//     });
// });

