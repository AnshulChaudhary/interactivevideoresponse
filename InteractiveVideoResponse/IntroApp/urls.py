from django.urls import path, include
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.intro, name='intro'),
    path('accounts/logout', views.intro, name='intro'),
]