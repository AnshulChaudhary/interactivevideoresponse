from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

# Create your views here.

def intro(request):
    return render(request, 'account/login.html')

