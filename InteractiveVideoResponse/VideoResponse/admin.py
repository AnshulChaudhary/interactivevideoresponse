from django.contrib import admin

from .models import submission_attempts, Uploaded_video
# Register your models here.

class submission_attemptsAdmin(admin.ModelAdmin):
    list_display = ('candidate_id', 'interview_questions', 'interview_id', 'current_attempts', 'max_attempts', 'response_submitted')
    search_fields = ()
    ordering = ('interview_id', 'candidate_id',)
    list_filter = ('response_submitted','interview_id',)

admin.site.register(submission_attempts, submission_attemptsAdmin)
admin.site.register(Uploaded_video)
