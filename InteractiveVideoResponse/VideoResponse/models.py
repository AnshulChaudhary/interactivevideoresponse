from django.db import models

from CompanyDashboard.models import candidate_details, interview_details, interview_questions
from account.models import company_details

# Create your models here.


# class interview_discription(models.Model):
#     pass

class submission_attempts(models.Model):
    max_attempts = models.IntegerField(default=1)
    current_attempts = models.IntegerField(default=0)
    response_submitted = models.BooleanField(default=False)
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)
    candidate_id = models.ForeignKey(candidate_details,on_delete=models.CASCADE)
    interview_questions = models.ForeignKey(interview_questions,on_delete=models.CASCADE)



class Uploaded_video(models.Model):
    video_name = models.CharField(max_length=255)
    video_response = models.CharField(max_length=255)
    candidate_id = models.ForeignKey(candidate_details,on_delete=models.CASCADE)
    interview_id = models.ForeignKey(interview_details,on_delete=models.CASCADE)
    interview_questions = models.ForeignKey(interview_questions,on_delete=models.CASCADE)

    def __str__(self):
        return self.video_name

    

