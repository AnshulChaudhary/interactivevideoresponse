let mediaRecorder;
let recordedBlobs;

const recordedVideo = document.querySelector('video#recorded');
const recordButton = document.querySelector('button#record');
const viewResponseButton = document.querySelector('button#viewResponse');
const playPauseButton = document.querySelector('button#playPause');

// call functions on load 
window.onload = function() {
    startCamera()
    preparationTimeCountdown()
};


// begin preparation countdown 
    var preparationTimer;
    function preparationTimeCountdown() {
        var timeleft  = parseInt(document.getElementById("preparationTime").getAttribute('value'),10)+5;
        preparationTimer = setInterval(myClock, 1000);
        function myClock() {
            timeleft = --timeleft
            document.getElementById("preparationTime").innerHTML = "Time left: " + timeleft;
            if (timeleft == 0) {
                $('#gum')[0].style.opacity = 1;
                clearInterval(preparationTimer)
                $('#preparationTime').css('display','none');
                $('#interviewTime').css('display','inline');
                InterviewTimeCountdown();
                startRecording();
            }
        }
    }
// end preparation countdown 


// begin interview countdown 
    var InterviewTimer;
    function InterviewTimeCountdown() {
        InterviewTimer = setInterval(myClock, 1000);
        var timeleft  = document.getElementById("interviewTime").getAttribute('value');
        function myClock() {
            timeleft = --timeleft
            document.getElementById("interviewTime").innerHTML = "Time left: " + timeleft;
            if (timeleft == 0) {
                clearInterval(InterviewTimer);
                stopRecording();
                $('#interviewTime').css('display','none');
                $('#record').css('display','none');
            }
        }
    }
// end interview countdown 


// begin start camera 

    function handleSuccess(stream) {
        window.stream = stream;
        const gumVideo = document.querySelector('video#gum');
        gumVideo.srcObject = stream;
    }

    async function init(constraints) {
        try {
            const stream = await navigator.mediaDevices.getUserMedia(constraints);
            handleSuccess(stream);
        } catch (e) {
                console.error('navigator.getUserMedia error:', e);
                alert(`navigator.getUserMedia \n error: ${e.toString()}`);
            }
    }

    async function startCamera(){
            const constraints = {
                audio: true,
                video: {
                width: 1280, height: 720
                }
            };
            await init(constraints);
    }

// end start camera


// begin recording(start stop)

    recordButton.addEventListener('click', () => {
        if (recordButton.textContent === 'Start Recording') {
            $('#gum').css('opacity', 1);
            clearInterval(preparationTimer)
            $('#preparationTime').css('display','none');
            $('#interviewTime').css('display','inline');
            InterviewTimeCountdown();
            startRecording();
        } else {
            stopRecording();
            clearInterval(InterviewTimer);
            $('#interviewTime').css('display','none');
            $('#record').css('display','none');
        }
    });

    function handleDataAvailable(event) {
        if (event.data && event.data.size > 0) {
            recordedBlobs.push(event.data);
        }
    }

    function startRecording() {
        recordedBlobs = [];
        let options = {mimeType: 'video/webm'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            alert(`${options.mimeType} is not supported`);
        }

        try {
            mediaRecorder = new MediaRecorder(window.stream, options);
        } catch (e) {
            alert('Exception while creating MediaRecorder:', e);
            return;
        }
        recordButton.textContent = 'Stop Recording';
        
        mediaRecorder.onstop = (event) => {
        };
        mediaRecorder.ondataavailable = handleDataAvailable;
        mediaRecorder.start();
    }

    function stopRecording() {
        mediaRecorder.stop();
        stream.getTracks().forEach(function(mediaRecorder) {
            if (mediaRecorder.readyState == 'live') {
                mediaRecorder.stop();
            }
        });
        $('#viewResponse').css('display','inline');
        $('#submitVideo').css('display','inline');
        $('#gum').css('opacity', 0.3);
    }

// end recording 


// begin view response 
    viewResponseButton.addEventListener('click', () => {
        $('#gum').css('display', 'none');
        $('#recorded').css('display','inline');
        $('#playPause').css('display','inline');
        $('#viewResponse').css('display','none');
        const superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
        recordedVideo.src = null;
        recordedVideo.srcObject = null;
        recordedVideo.src = window.URL.createObjectURL(superBuffer);
        recordedVideo.controls = false;
    });
// end view response 


// begin play pause
    playPauseButton.addEventListener('click', () => {
        if (playPauseButton.textContent === 'Play'){
            recordedVideo.play();
            playPauseButton.textContent = 'Pause';
        } else {
            recordedVideo.pause();
            playPauseButton.textContent = 'Play';
        }
    });
// end play pause


// video played once 
    recordedVideo.addEventListener('ended', () =>{
        $('#recorded').css('opacity',0.3);
        playPauseButton.textContent = 'Play';
        playPauseButton.disabled = true;
    });


// video duration
// var vid = document.getElementById("recorded");
// console.log(vid.duration);