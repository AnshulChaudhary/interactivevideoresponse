from django.urls import path, include
from django.conf.urls import url

from . import views

urlpatterns = [
    # interview recording 
    path('interview/record/<interview_code>', views.interviewIntro, name='Interview Intro'),
    path('interview/record/<interview_code>/InterviewRecording', views.interviewRecording, name='Interview Recording'),
    path('interview/record/<interview_code>/InterviewRecording/<question>/submitResponse', views.SumbitResponse, name='Submit Response'),
    # demo interview
    path('interview/record/<interview_code>/demo', views.demoInterviewIntro, name='Demo Interview Intro'),
    path('interview/record/<interview_code>/demo/response', views.demoInterview, name='Demo Interview'),
    # test camera mic
    path('interview/record/<interview_code>/TestCameraMic', views.testCameraMic, name='Test Camera Mic'),
    
]