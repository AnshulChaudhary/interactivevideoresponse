from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, auth
from django.contrib import messages

from account.models import company_details
from CompanyDashboard.models import interview_setup_validate, candidate_details, interview_details, interview_questions, interview_interviewer_map
from .models import submission_attempts, Uploaded_video
import sys
import os
import json
from sys import getsizeof
from ipware import get_client_ip
import random

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Create your views here.

# intor of interview
def interviewIntro(request, interview_code):
    if request.method == 'GET':
        if candidate_details.objects.filter(interview_code=interview_code).exists():
            candidate = candidate_details.objects.get(interview_code=interview_code)
            if candidate.link_status:
                if not candidate.submission_status:
                    return render(request, 'response/interviewIntro.html', {'interview': candidate.interview_id, 'candidate': candidate})
                else:
                    return render(request, 'response/linkStatus/responseSubmitted.html')
            else:
                return render(request, 'response/linkStatus/linkExpired.html')
        else:
            return render(request, 'response/linkStatus/invalidLink.html')


# main interview 
def interviewRecording(request, interview_code):
    if request.method == 'GET':
        if candidate_details.objects.filter(interview_code=interview_code).exists():
            candidate = candidate_details.objects.get(interview_code=interview_code)
            if candidate.link_status:
                if not candidate.submission_status:
                    if submission_attempts.objects.filter(interview_id=candidate.interview_id, candidate_id= candidate, response_submitted = False).exists():
                        questions = submission_attempts.objects.filter(interview_id=candidate.interview_id, candidate_id= candidate, response_submitted = False)
                        question = []
                        for i in questions:
                            question.append(i)
                        remaining_attempt = question[0].max_attempts - question[0].current_attempts - 1
                        return render(request, 'response/recordResponse/interviewRecording.html', {'question': question[0], 'remaining_attempt': remaining_attempt})
                    else:
                        return render(request, 'response/linkStatus/responseSubmitted.html')
                else:
                    return render(request, 'response/linkStatus/responseSubmitted.html')
            else:
                return render(request, 'response/linkStatus/linkExpired.html')
        else:
            return render(request, 'response/linkStatus/invalidLink.html')


#upload candidate video
def SumbitResponse(request, interview_code, question):
    if request.method == 'POST':
        video = request.body
        candidate = candidate_details.objects.get(interview_code=interview_code)
        ques = interview_questions.objects.get(interview_id=candidate.interview_id, interview_question=question)
        # upload video
        # ********** change ques.id with question_nuber ****************
        temp_path = 'media\\'+ candidate.interview_id.company_id.company_name +'\\'+ candidate.interview_id.interview_name +'\\Interview Responses\\'+ candidate.first_name + ' ' + candidate.last_name
        video_path = os.path.join(BASE_DIR, temp_path)
        if not os.path.exists(video_path):
            os.makedirs(video_path)
        temp_file = str(ques.id) + '.webm'
        video_file = os.path.join(video_path, temp_file)
        f = open(video_file, 'wb')
        f.write(video)
        f.close()
        # save video location in database
        obj = Uploaded_video.objects.create(
            video_name = ques.id,
            video_response = video_file,
            candidate_id = candidate,
            interview_id = candidate.interview_id,
            interview_questions = ques
        )
        obj.save()
        # update the attempts        
        attempt = submission_attempts.objects.get(interview_id=candidate.interview_id, candidate_id= candidate, interview_questions= ques)
        attempt.current_attempts = attempt.current_attempts + 1
        attempt.response_submitted = True
        attempt.save()
        # update candidate details
        ip_address, is_routable = get_client_ip(request)
        candidate.question_answered = candidate.question_answered +1
        candidate.candidate_ip = ip_address
        candidate.save()
        # check if question is the last question
        questions = interview_questions.objects.filter(interview_id=candidate.interview_id)    
        if questions[len(questions)-1].interview_question == question and candidate.question_answered == len(questions):
            candidate.submission_status = True
            candidate.save()
        
        return HttpResponse('')



# demo interview intro
def demoInterviewIntro(request, interview_code):
    if request.method == 'GET':
        return render(request, 'demoResponse/DemoResponseIntro.html', {'interview_code':interview_code})
    
    
# demo interview
def demoInterview(request, interview_code):
    if request.method == 'GET':
        questions = interview_questions.objects.filter(interview_id = 1)
        i = random.randint(0, 3)
        return render(request, 'demoResponse/DemoResponse.html', {'interview_code':interview_code, 'question':questions[i]})


# test camera mic
def testCameraMic(request, interview_code):
    if request.method == 'GET':
        return render(request, 'testCameraMic/test.html')
