from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class company_details(models.Model):
    company_name = models.CharField(max_length=255, unique=True)
    live_interview = models.IntegerField(default=0)
    total_interview = models.IntegerField(default=0)

    def __str__(self):
        return self.company_name

class MyUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, company_id, password, is_coordinator):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            company_id=company_id,
            is_coordinator=is_coordinator,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        
        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.is_coordinator=True
        user.is_admin = True
        user.is_staff = True
        user.is_interviewer = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    company_id = models.ForeignKey('company_details', null=True, default=None,on_delete=models.CASCADE)
    is_coordinator = models.BooleanField(default=False)
    is_interviewer = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True


class Confirmation_OTP(models.Model):
    user_id = models.ForeignKey('MyUser',on_delete=models.CASCADE)
    otp = models.IntegerField()

