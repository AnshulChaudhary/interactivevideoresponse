from django.urls import path, include
from django.conf.urls import url

from . import views

urlpatterns = [

    # login
    path('accounts/login/', views.login, name='login page'),
    path('accounts/signin/', views.signin, name='signin'),
    # logout
    path('accounts/logout/', views.logout, name='logout'),
    # first login reset
    path('accounts/confirmAccount/', views.confirmAccount, name='confirm account'),
    path('accounts/resendConfirmationOTP/', views.resendConfirmationOTP, name='Resend confirmation OTP'),
    path('accounts/resetPassword/', views.resetPassword, name='Reset Password'),
]