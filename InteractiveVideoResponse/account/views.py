from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, auth
from django.contrib import messages

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

import random, math

from django.contrib.auth.decorators import login_required


from .models import  Confirmation_OTP, company_details


#create company 
# def create_company():
#     company = company_details.objects.create(
#         company_name = 'The Black Fox Consulting',
#         live_interview = 0,
#         total_interview = 0,
#     )
# if not company_details.objects.filter(company_name = 'The Black Fox Consulting').exists():
#     create_company()

# login/signup
def login(request):
    return render(request, 'account/login.html')

def signin(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(email=email, password=password)
        if user is not None:
            if user.last_login is None:
                auth.login(request, user)
                user.last_login = None
                user.save()

                # generate OTP
                OTP = GenerateOTP()

                # add OTP to db
                if Confirmation_OTP.objects.filter(user_id=user).exists():
                    conf_OTP =  Confirmation_OTP.objects.get(user_id=user)
                    conf_OTP.otp = OTP
                    conf_OTP.save()
                else:
                    new_OTP = Confirmation_OTP.objects.create(user_id=user, otp=OTP)
                    new_OTP.save()
                    
                # end db

                # send email
                plaintext = get_template('account/firstLogin.txt')
                d = { 'OTP': OTP, 'fname': user.first_name, 'lname': user.last_name}
                subject = 'To Reset Password' 
                from_email = 'noreply@example.com'
                to_email = [user.email]
                text_content = plaintext.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
                msg.send()
                # end email
                
                return redirect('/accounts/confirmAccount/')
            else:
                auth.login(request, user)
                return redirect('/')
        else:
            messages.info(request, 'Invalid username or password')
            return redirect('/')

@login_required
def confirmAccount(request):
    user = request.user
    if request.method == 'POST':
        conf_OTP = Confirmation_OTP.objects.get(user_id=user)
        entered_otp = request.POST['otp']
        entered_otp = int(entered_otp)
        if entered_otp == conf_OTP.otp:
            conf_OTP.delete()
            return render(request, 'account/resetPassword.html')
        else:
            messages.info(request, 'Invalid OTP, Please use correct OTP')
            return redirect('/accounts/confirmAccount/')
    else:
        return render(request, 'account/confirmAccount.html')

@login_required
def resendConfirmationOTP(request):
    OTP = GenerateOTP()
    user = request.user
    if Confirmation_OTP.objects.filter(user_id=user).exists():
        conf_OTP =  Confirmation_OTP.objects.get(user_id=user)
        conf_OTP.otp = OTP
        conf_OTP.save()
    else:
        new_OTP = Confirmation_OTP.objects.create(user_id=user, otp=OTP)
        new_OTP.save()
    # send email
    plaintext = get_template('account/firstLogin.txt')
    d = { 'OTP': OTP, 'fname': user.first_name, 'lname': user.last_name}
    subject = 'To Reset Password' 
    from_email = 'noreply@example.com'
    to_email = [user.email]
    text_content = plaintext.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
    msg.send()
    # end email

    messages.info(request, 'New OTP sent')
    return redirect('/accounts/confirmAccount/')

@login_required
def resetPassword(request):
    pass1 = request.POST['password1']
    pass2 = request.POST['password2']
    user = request.user
    if pass1 == pass2:
        user.set_password(pass1)
        user.save()
        auth.login(request, user)
        auth.logout(request)
        messages.info(request, 'Reset Password Successful!')
        return redirect('/')
    else:
        messages.info(request, 'Passwords did not match')
        return redirect('/accounts/resetPassword/')

@login_required
def logout(request):
    auth.logout(request)
    return redirect('/')



def GenerateOTP():
    digits = [i for i in range(0, 10)]
    OTP_temp = ""
    for i in range(0,6):
        index = math.floor(random.random() * 10)
        OTP_temp += str(digits[index])
    OTP = int(OTP_temp)
    return(OTP)