# Video Interview Site

This repository contains two websites built using the Django framework:

1. **Company Website**: This website serves as the online presence of the company and showcases the work and services offered by the organization. It provides information about the company's mission, vision, team, and projects. The company website does not involve a database and primarily focuses on presenting the company's portfolio.

2. **Interview Recording Web Application**: The second website is an interactive web application designed for conducting and recording online interviews. It utilizes a SQL database to manage user accounts, interview recordings, and related data. This application is a valuable tool for organizations looking to streamline their interview processes and maintain digital records.

## Company Website

### Features
- Overview of the company's mission and values.
- Detailed information about the team members and their roles.
- Showcase of past projects and achievements.
- Contact information for inquiries and collaboration.

### Technologies Used
- Django for web application development.
- HTML and CSS for the user interface.
- JavaScript for interactive elements.

## Interview Recording Web Application

### Features
- User Authentication: Secure user registration and login.
- Interview Recording: Ability to initiate and record interviews online.
- Database Management: SQL database to store user accounts, interview recordings, and relevant information.
- User Profiles: Personalized user profiles with the option to edit details.
- Admin Panel: Administrative access to manage users, recordings, and system settings.
- There is a view you the company to rate the interview.

### Technologies Used
- Django for web application development.
- SQL database for data storage.
- HTML, CSS, and JavaScript for the user interface.
- Django Admin for administrative functionalities.
